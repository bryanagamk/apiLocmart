<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Store */

$this->title = 'Update Store: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->STORE_ID, 'url' => ['view', 'id' => $model->STORE_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
