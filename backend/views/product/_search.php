<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PRODUCT_ID') ?>

    <?= $form->field($model, 'CATEGORY_ID') ?>

    <?= $form->field($model, 'STORE_ID') ?>

    <?= $form->field($model, 'PRODUCT_NAME') ?>

    <?= $form->field($model, 'PRODUCT_PRICE') ?>

    <?php // echo $form->field($model, 'PRODUCT_DESCRIPTION') ?>

    <?php // echo $form->field($model, 'PRODUCT_IMG') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
