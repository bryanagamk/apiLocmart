<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$this->title = $model->PRODUCT_ID;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->PRODUCT_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->PRODUCT_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PRODUCT_ID',
            'CATEGORY_ID',
            'STORE_ID',
            'PRODUCT_NAME',
            'PRODUCT_PRICE',
            'PRODUCT_DESCRIPTION:ntext',
            //'PRODUCT_IMG',
            [
                'attribute'=>'photo',
                'value'=>Yii::getAlias('@web').'/'.$model->PRODUCT_IMG,
                'format' => ['image'],
            ]
        ],
    ]) ?>

</div>
