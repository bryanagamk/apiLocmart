<?php

use backend\models\Categories;
use backend\models\Store;
use kartik\money\MaskMoney;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'PRODUCT_ID')->textInput() ?>

    <?= $form->field($model, 'CATEGORY_ID')->dropDownList(
        ArrayHelper::map(Categories::find()->all(),'CATEGORY_ID','CATEGORY_NAME'),
        ['prompt' => 'Select Categories Product']
    );?>

    <?= $form->field($model, 'STORE_ID')->dropDownList(
        ArrayHelper::map(Store::find()->all(),'STORE_ID','STORE_NAME'),
        ['prompt' => 'Select Store']
    );?>

    <?= $form->field($model, 'PRODUCT_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PRODUCT_PRICE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PRODUCT_DESCRIPTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PRODUCT_IMG')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
