<?php

use backend\models\Store;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Promo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'PROMO_ID')->textInput() ?>

    <?= $form->field($model, 'STORE_ID')->dropDownList(
        ArrayHelper::map(Store::find()->all(),'STORE_ID','STORE_NAME'),
        ['prompt' => 'Select Store']
    );?>

    <?= $form->field($model, 'PROMO_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PROMO_IMG')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
