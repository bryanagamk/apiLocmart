<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\StoreLocation */

$this->title = 'Update Store Location: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Store Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NAME, 'url' => ['view', 'id' => $model->LOCATION_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="store-location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
