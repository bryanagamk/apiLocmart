<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\StoreLocation */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => 'Store Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-location-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->LOCATION_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->LOCATION_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'LOCATION_ID',
            'STORE_ID',
            'NAME',
            'ADDRESS',
            'COORDINATE',
            'PHONE',
        ],
    ]) ?>

</div>
