<?php

use backend\models\Store;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\StoreLocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-location-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'LOCATION_ID')->textInput() ?>

    <?= $form->field($model, 'STORE_ID')->dropDownList(
        ArrayHelper::map(Store::find()->all(),'STORE_ID','STORE_NAME'),
        ['prompt' => 'Select Store']
    );?>

    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADDRESS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COORDINATE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PHONE')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
