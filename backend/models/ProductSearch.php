<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PRODUCT_ID', 'CATEGORY_ID', 'STORE_ID'], 'integer'],
            [['PRODUCT_NAME', 'PRODUCT_PRICE', 'PRODUCT_DESCRIPTION', 'PRODUCT_IMG'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'pageSize' => 200,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PRODUCT_ID' => $this->PRODUCT_ID,
            'CATEGORY_ID' => $this->CATEGORY_ID,
            'STORE_ID' => $this->STORE_ID,
        ]);

        $query->andFilterWhere(['like', 'PRODUCT_NAME', $this->PRODUCT_NAME])
            ->andFilterWhere(['like', 'PRODUCT_PRICE', $this->PRODUCT_PRICE])
            ->andFilterWhere(['like', 'PRODUCT_DESCRIPTION', $this->PRODUCT_DESCRIPTION])
            ->andFilterWhere(['like', 'PRODUCT_IMG', $this->PRODUCT_IMG]);

        return $dataProvider;
    }
}
