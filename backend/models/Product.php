<?php

namespace backend\models;

use backend\models\Categories;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $PRODUCT_ID
 * @property int $CATEGORY_ID
 * @property int $STORE_ID
 * @property string $PRODUCT_NAME
 * @property string $PRODUCT_PRICE
 * @property string $PRODUCT_DESCRIPTION
 * @property string $PRODUCT_IMG
 *
 * @property Categories $cATEGORY
 * @property Store $store
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public static function primaryKey()
    {
        return ['PRODUCT_ID'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PRODUCT_ID', 'CATEGORY_ID', 'STORE_ID'], 'required'],
            [['PRODUCT_ID', 'CATEGORY_ID', 'STORE_ID'], 'integer'],
            [['PRODUCT_DESCRIPTION'], 'string'],
            [['PRODUCT_NAME', 'PRODUCT_PRICE'], 'string', 'max' => 255],
            [['PRODUCT_IMG'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['PRODUCT_ID'], 'unique'],
            [['CATEGORY_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['CATEGORY_ID' => 'CATEGORY_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PRODUCT_ID' => 'Product  ID',
            'CATEGORY_ID' => 'Categories  ID',
            'STORE_ID' => 'Store  ID',
            'PRODUCT_NAME' => 'Product  Name',
            'PRODUCT_PRICE' => 'Product  Price',
            'PRODUCT_DESCRIPTION' => 'Product  Description',
            'PRODUCT_IMG' => 'Product  Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCATEGORY()
    {
        return $this->hasOne(Categories::className(), ['CATEGORY_ID' => 'CATEGORY_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['STORE_ID' => 'STORE_ID']);
    }
}
