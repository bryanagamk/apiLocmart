<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "store_location".
 *
 * @property int $LOCATION_ID
 * @property int $STORE_ID
 * @property string $NAME
 * @property string $ADDRESS
 * @property string $COORDINATE
 * @property string $PHONE
 *
 * @property Store $sTORE
 */
class StoreLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LOCATION_ID', 'NAME'], 'required'],
            [['LOCATION_ID', 'STORE_ID'], 'integer'],
            [['NAME'], 'string', 'max' => 50],
            [['ADDRESS', 'COORDINATE', 'PHONE'], 'string', 'max' => 255],
            [['LOCATION_ID'], 'unique'],
            [['STORE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['STORE_ID' => 'STORE_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'LOCATION_ID' => 'Location  ID',
            'STORE_ID' => 'Store  ID',
            'NAME' => 'Name',
            'ADDRESS' => 'Address',
            'COORDINATE' => 'Coordinate',
            'PHONE' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTORE()
    {
        return $this->hasOne(Store::className(), ['STORE_ID' => 'STORE_ID']);
    }
}
