<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property int $STORE_ID
 * @property string $STORE_NAME
 * @property string $STORE_IMG
 *
 * @property Product[] $products
 * @property Promo[] $promos
 * @property StoreLocation[] $storeLocations
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STORE_ID'], 'required'],
            [['STORE_ID'], 'integer'],
            [['STORE_NAME'], 'string', 'max' => 255],
            [['STORE_IMG'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['STORE_ID'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'STORE_ID' => 'Store  ID',
            'STORE_NAME' => 'Store  Name',
            'STORE_IMG' => 'Store  Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['STORE_ID' => 'STORE_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['STORE_ID' => 'STORE_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreLocations()
    {
        return $this->hasMany(StoreLocation::className(), ['STORE_ID' => 'STORE_ID']);
    }
}
