<?php
/**
 * Created by PhpStorm.
 * User: Escanor
 * Date: 1/3/2018
 * Time: 10:34 AM
 */

namespace app\models;


use yii\base\Model;

class UploadForm extends Model
{
    /**
    * @var UploadedFile
    */

    public $ImageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}