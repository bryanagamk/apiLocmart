<?php

namespace backend\models;

use backend\models\Product;
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $CATEGORY_ID
 * @property string $CATEGORY_NAME
 *
 * @property Product[] $products
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CATEGORY_ID'], 'required'],
            [['CATEGORY_ID'], 'integer'],
            [['CATEGORY_NAME'], 'string', 'max' => 255],
            [['CATEGORY_ID'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CATEGORY_ID' => 'Categories  ID',
            'CATEGORY_NAME' => 'Categories  Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['CATEGORY_ID' => 'CATEGORY_ID']);
    }
}
