<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "promo".
 *
 * @property int $PROMO_ID
 * @property int $STORE_ID
 * @property string $PROMO_NAME
 * @property string $PROMO_IMG
 *
 * @property Store $sTORE
 */
class Promo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PROMO_ID'], 'required'],
            [['PROMO_ID', 'STORE_ID'], 'integer'],
            [['PROMO_NAME'], 'string', 'max' => 255],
            [['PROMO_IMG'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['PROMO_ID'], 'unique'],
            [['STORE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['STORE_ID' => 'STORE_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PROMO_ID' => 'Promo  ID',
            'STORE_ID' => 'Store  ID',
            'PROMO_NAME' => 'Promo  Name',
            'PROMO_IMG' => 'Promo  Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTORE()
    {
        return $this->hasOne(Store::className(), ['STORE_ID' => 'STORE_ID']);
    }
}
