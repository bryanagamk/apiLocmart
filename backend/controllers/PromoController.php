<?php

namespace backend\controllers;

use Yii;
use backend\models\Promo;
use backend\models\PromoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PromoController implements the CRUD actions for Promo model.
 */
class PromoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Promo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Promo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Promo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Promo();

        if ($model->load(Yii::$app->request->post())) {
            $model->PROMO_IMG = UploadedFile::getInstance($model, 'PROMO_IMG');

            $image_name_temp = $model->PROMO_NAME.rand(1, 4000) . '.' . $model->PROMO_IMG->getExtension();
            $image_name = str_replace(' ','',$image_name_temp);
            $image_path = 'uploads/store/' . $image_name;

            $model->PROMO_IMG->saveAs($image_path);
            $model->PROMO_IMG = $image_path;

            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->PROMO_ID]);
            }else{
                echo 'error';
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Promo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->PROMO_IMG = UploadedFile::getInstance($model, 'PROMO_IMG');

            $image_name = $model->PROMO_NAME.rand(1, 4000) . '.' . $model->PROMO_IMG->getExtension();
            $image_path = 'uploads/store/' . $image_name;

            $model->PROMO_IMG->saveAs($image_path);
            $model->PROMO_IMG = $image_path;

            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->PROMO_ID]);
            }else{
                echo 'error';
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Promo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Promo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Promo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Promo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
