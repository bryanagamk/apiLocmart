<?php
return [
    'adminEmail' => 'admin@example.com',
    'maskMoneyOptions' => [
        'prefix' => 'IDR ',
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 0,
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
