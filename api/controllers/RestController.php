<?php
/**
 * Created by PhpStorm.
 * User: Escanor
 * Date: 1/5/2018
 * Time: 12:28 AM
 */
namespace api\controllers;

use yii\rest\Controller;

class RestController extends Controller
{
    private function dataList()
    {
        return [
            [ 'id' => 1, 'name' => 'Albert', 'surname' =>
                'Einstein' ],
            [ 'id' => 2, 'name' => 'Enzo', 'surname' => 'Ferrari'
            ],
            [ 'id' => 4, 'name' => 'Mario', 'surname' => 'Bros' ]
        ];
    }
    public function actionIndex()
    {
        return $this->dataList();
    }
}