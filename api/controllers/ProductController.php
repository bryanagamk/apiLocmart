<?php
/**
 * Created by PhpStorm.
 * User: Escanor
 * Date: 1/5/2018
 * Time: 1:58 AM
 */

namespace api\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ProductController extends ActiveController
{
    public $modelClass = 'backend\models\ProductSearch';

    public function actionSearch()
    {
        if (!empty($_GET)) {
            $model = new $this->modelClass;
            foreach ($_GET as $key => $value) {
                if (is_array($value))
                    foreach ($value as $keyVal => $subValue) {
                        if (!$model->hasAttribute($keyVal)) {
                            throw new \yii\web\HttpException(404, 'Invalid attribute:' . $keyVal);
                        }
                    }
            }
            try {
                $provider = $model->search(
                    Yii::$app->request->queryParams);/*new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
                    'pagination' => false
                ]);*/
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }

            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            throw new \yii\web\HttpException(400, 'There are no query string');
        }
    }

}