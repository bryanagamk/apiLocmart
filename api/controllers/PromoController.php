<?php
/**
 * Created by PhpStorm.
 * User: Escanor
 * Date: 1/5/2018
 * Time: 1:29 AM
 */

namespace api\controllers;

use Yii;
use yii\rest\ActiveController;

class PromoController extends ActiveController
{
    public $modelClass = 'backend\models\PromoSearch';

    public function actionSearch()
    {
        if (!empty($_GET)) {
            $model = new $this->modelClass;
            foreach ($_GET as $key => $value) {
                if (is_array($value))
                    foreach ($value as $keyVal => $subValue) {
                        if (!$model->hasAttribute($keyVal)) {
                            throw new \yii\web\HttpException(404, 'Invalid attribute:' . $keyVal);
                        }
                    }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->search()->where($_GET),
                    'pagination' => false,
                ]);

            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }

            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            throw new \yii\web\HttpException(400, 'There are no query string');
        }
    }

}